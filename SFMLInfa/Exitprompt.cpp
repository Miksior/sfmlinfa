#include "Exitprompt.h"
#include "Consts.h"

Exitprompt::Exitprompt(float width, float height){

	font.loadFromFile(FONT);

	redraw(width, height);

	selectedItemIndex = 0;
}

void Exitprompt::draw(sf::RenderWindow & window){

	for (int i = 0; i < 2; i++)
	{
		window.draw(menu[i]);
	}
}

void Exitprompt::moveUp() {

	if (selectedItemIndex - 1 >= 0)
	{
		menu[selectedItemIndex].setFillColor(sf::Color::White);
		selectedItemIndex--;
		menu[selectedItemIndex].setFillColor(sf::Color::Red);
	}
}

void Exitprompt::moveDown() {

	if (selectedItemIndex + 1 < 2)
	{
		menu[selectedItemIndex].setFillColor(sf::Color::White);
		selectedItemIndex++;
		menu[selectedItemIndex].setFillColor(sf::Color::Red);
	}
}

void Exitprompt::redraw(float width, float height) {

	menu[0].setFont(font);
	menu[0].setFillColor(sf::Color::White);
	menu[0].setString("Tak, chce wyjsc.");
	menu[0].setPosition(sf::Vector2f(width / 3, height / 3 * 1));

	menu[1].setFont(font);
	menu[1].setFillColor(sf::Color::White);
	menu[1].setString("Nie, nie chce wychodzic");
	menu[1].setPosition(sf::Vector2f(width / 3, height / 3 * 2));

	menu[selectedItemIndex].setFillColor(sf::Color::Red);
}

