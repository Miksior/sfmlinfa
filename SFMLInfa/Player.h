#pragma once
#include "Object.h"
#include "Consts.h"

class Player : public Object{
public:
	bool thrust;
	int lifes = 30;
	int points = 0;

	Player(){
		name = "player";
	}

	void update(){
		if (thrust){
			dx += cos(angle*DTR)*0.2;
			dy += sin(angle*DTR)*0.2;
		}
		else{
			dx *= 0.99;
			dy *= 0.99;
		}

		int maxSpeed = 15;
		float speed = sqrt(dx*dx + dy * dy);
		if (speed>maxSpeed){
			dx *= maxSpeed / speed;
			dy *= maxSpeed / speed;
		}

		x += dx;
		y += dy;

		if (x>WIDTH) x = 0; if (x<0) x = WIDTH;
		if (y>HEIGHT) y = 0; if (y<0) y = HEIGHT;
	}

};