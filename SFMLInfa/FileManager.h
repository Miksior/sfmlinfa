#pragma once
#include "Player.h"
#include <fstream>
#include <string>
#include <iostream>

class FileManager
{
public:
	std::fstream file;
	std::string filename;
	std::string filepath;

	FileManager(std::string fn, std::string fp="") {
		filename = fn;
		filepath = fp;
	}

	void save(int p, int l) {
		file.open(filepath+filename, std::ios::out);
		if (file.good())
		{
			file << l << " " << p;
			file.close();
			std::cout << "Zapisano" << std::endl;
		}
		else std::cout << "Brak pliku" << std::endl;
	}

	void load(Player* p) {
		file.open(filepath + filename, std::ios::in | std::ios::out);
		if (file.good())
		{
			std::string ll, pp;
			file >> ll >> pp;
			p->points = std::stoi(pp);
			p->lifes = std::stoi(ll);

			file.close();
			std::cout << "Wczytano" << std::endl;
		}
		else std::cout << "Brak pliku" << std::endl;
	}
};

