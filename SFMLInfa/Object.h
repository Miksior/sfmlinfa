#pragma once
#include "Animation.h"
#include <SFML/Graphics.hpp>

class Object
{
public:
	float x, y, dx, dy, R, angle;
	bool life;
	std::string name;
	Animation anim;

	Object();

	void settings(Animation &a, int X, int Y, float Angle = 0, int radius = 1);

	virtual void update() {};

	void draw(RenderWindow &app);

	virtual ~Object() {};
};
