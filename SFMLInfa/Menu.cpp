#include <SFML/Graphics.hpp>
#include "Menu.h"
#include "Consts.h"

Menu::Menu(float width, float height){

	font.loadFromFile(FONT);

	redraw(width, height);

	selectedItemIndex = 0;
}


void Menu::draw(sf::RenderWindow &window){
	
	for (int i = 0; i < MAX_NUMBER_OF_ITEMS; i++){
		window.draw(menu[i]);
	}
}

//Poruszanie wskaznikiem menu
void Menu::moveUp(){

	if (selectedItemIndex - 1 >= 0){
		
		menu[selectedItemIndex].setFillColor(sf::Color::White);
		selectedItemIndex--;
		menu[selectedItemIndex].setFillColor(sf::Color::Green);
	}
}

void Menu::moveDown(){

	if (selectedItemIndex + 1 < MAX_NUMBER_OF_ITEMS){
		
		menu[selectedItemIndex].setFillColor(sf::Color::White);
		selectedItemIndex++;
		menu[selectedItemIndex].setFillColor(sf::Color::Green);
	}
}

void Menu::redraw(float width, float height){

	menu[0].setFont(font);
	menu[0].setFillColor(sf::Color::White);
	menu[0].setString("Graj");
	menu[0].setPosition(sf::Vector2f(width / 3, height / (MAX_NUMBER_OF_ITEMS + 1) * 1));

	menu[1].setFont(font);
	menu[1].setFillColor(sf::Color::White);
	menu[1].setString("Zapisz gre");
	menu[1].setPosition(sf::Vector2f(width / 3, height / (MAX_NUMBER_OF_ITEMS + 1) * 2));

	menu[2].setFont(font);
	menu[2].setFillColor(sf::Color::White);
	menu[2].setString("Wczytaj gre");
	menu[2].setPosition(sf::Vector2f(width / 3, height / (MAX_NUMBER_OF_ITEMS + 1) * 3));

	menu[3].setFont(font);
	menu[3].setFillColor(sf::Color::White);
	menu[3].setString("Poziom trudnosci: " + std::to_string(level));
	menu[3].setPosition(sf::Vector2f(width / 3, height / (MAX_NUMBER_OF_ITEMS + 1) * 4));

	menu[4].setFont(font);
	menu[4].setFillColor(sf::Color::White);
	menu[4].setString("Wyjdz");
	menu[4].setPosition(sf::Vector2f(width / 3, height / (MAX_NUMBER_OF_ITEMS + 1) * 5));

	menu[selectedItemIndex].setFillColor(sf::Color::Green);
}

//Wyswietlenie ekranu z informacjami
void Menu::showInfoScreen(sf::RenderWindow &window,float width, float height){
	
	sf::String info = "Poruszasz sie po mapie i strzelasz do asteroid.\nG�ra - poruszanie do przodu\n"
					  "Lewo/Prawo - obr�t\nF1 - informacje\nF2 - menu\nZmiana poziomu trudnosci polega na zmianie ilosci zyc.\n"
					  "Mozliwy jest jeden zapis stanu gry.\n\n"
					  "[Nacisnij F4 aby wrocic do gry]";
	sf::Text infoText;
	infoText.setFont(font);
	infoText.setFillColor(sf::Color::White);
	infoText.setString(info);
	infoText.setPosition(sf::Vector2f(width/10, height/3));

	window.draw(infoText);
}


