#pragma once
#include <SFML/Graphics.hpp>

#define MAX_NUMBER_OF_ITEMS 5

class Menu
{
public:
	Menu(float width, float height);
	bool show = false;
	bool showInfo = false;
	bool buttonAval = true;
	int level = 3;

	void draw(sf::RenderWindow &window);
	void moveUp();
	void moveDown();
	int getPressedItem() { return selectedItemIndex; }
	void redraw(float width, float height);
	void showInfoScreen(sf::RenderWindow &window, float width, float height);

private:
	int selectedItemIndex;
	sf::Font font;
	sf::Text menu[MAX_NUMBER_OF_ITEMS];
};

