#pragma once
#include <SFML/Graphics.hpp>

class Exitprompt
{
public:
	Exitprompt(float width, float height);
	bool show = false;
	void draw(sf::RenderWindow &window);
	void redraw(float width, float height);
	void moveUp();
	void moveDown();
	int getPressedItem() { return selectedItemIndex; }

private:
	int selectedItemIndex;
	sf::Font font;
	sf::Text menu[2];
};

