#pragma once
#include "Object.h"
#include "Consts.h"

class Bullet : public Object
{
public:
	Bullet(){
		name = "bullet";
	}

	void  update(){
		dx = cos(angle*DTR) * 6;
		dy = sin(angle*DTR) * 6;
		// angle+=rand()%6-3;
		x += dx;
		y += dy;

		if (x>WIDTH || x<0 || y>HEIGHT || y<0) life = 0;
	}

};