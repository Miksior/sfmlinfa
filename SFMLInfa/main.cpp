#include <SFML/Graphics.hpp>
#include <time.h>
#include <list>
#include <string>

#include "Consts.h"
#include "Animation.h"
#include "Object.h"
#include "Asteroid.h"
#include "Bullet.h"
#include "Player.h"
#include "Menu.h"
#include "Exitprompt.h"
#include "FileManager.h"

using namespace sf;

void switchBool(bool &b) {
	b = b ? false : true;
}

//Pomocnicza funkcja do badania czy obiekty sie ze soba zderzaja
bool isCollide(Object *a, Object *b) {
	return (b->x - a->x)*(b->x - a->x) +
		(b->y - a->y)*(b->y - a->y)<
		(a->R + b->R)*(a->R + b->R);
}

int main(){

	srand(time(0));

	RenderWindow app(VideoMode(WIDTH, HEIGHT), "Asteroidy");
	app.setFramerateLimit(60);

	Texture t1, t11, t2, t3, t4, t5, t6, t7;

	t1.loadFromFile("img/ship.png");
	t11.loadFromFile("img/ship_go.png");
	t2.loadFromFile("img/background.jpg");
	t3.loadFromFile("img/type_C.png");
	t4.loadFromFile("img/rock.png");
	t5.loadFromFile("img/fire_blue.png");
	t6.loadFromFile("img/rock_small.png");
	t7.loadFromFile("img/type_B.png");

	Sprite background(t2);

	Animation sExplosion(t3, 0, 0, 256, 256, 48, 0.5);
	Animation sRock(t4, 0, 0, 64, 64, 16, 0.2);
	Animation sRock_small(t6, 0, 0, 64, 64, 16, 0.2);
	Animation sBullet(t5, 0, 0, 32, 64, 16, 0.8);
	Animation sPlayer(t1, 0, 0, 40, 40, 1, 0);
	Animation sPlayer_go(t11, 0, 0, 40, 40, 1, 0);
	Animation sExplosion_ship(t7, 0, 0, 192, 192, 64, 0.5);

	std::list<Object*> objects;

	Player *p = new Player();
	p->settings(sPlayer, WIDTH/2, HEIGHT/2, 270, 20);
	objects.push_back(p);

	sf::Font font;
	font.loadFromFile(FONT);

	Text lifesText("Zycia: " + std::to_string(p->lifes), font);
	lifesText.setCharacterSize(30);
	lifesText.setStyle(sf::Text::Bold);
	lifesText.setFillColor(sf::Color::White);
	lifesText.setPosition(15, 10);

	Text pointsText("Punkty: " + std::to_string(p->points), font);
	pointsText.setCharacterSize(30);
	pointsText.setStyle(sf::Text::Bold);
	pointsText.setFillColor(sf::Color::White);
	pointsText.setPosition(20, 40);

	Menu menu(app.getSize().x, app.getSize().y);
	Exitprompt exitprompt(app.getSize().x, app.getSize().y);

	FileManager filemanager("safe.txt");

	bool paused = false;

	//Tworzenie asteroid
	for (int i = 0;i<ASTER;i++){
		Asteroid *a = new Asteroid();
		a->settings(sRock, rand() % WIDTH, rand() % HEIGHT, rand() % 360, 25);
		objects.push_back(a);
	}

	//Wypelnienie tla dla licznikow
	sf::ConvexShape convex;
	convex.setPointCount(5);
	convex.setPoint(0, sf::Vector2f(10, 8));
	convex.setPoint(1, sf::Vector2f(180, 10));
	convex.setPoint(2, sf::Vector2f(260, 80));
	convex.setPoint(3, sf::Vector2f(20, 90));
	convex.setPoint(4, sf::Vector2f(5, 60));
	convex.setOutlineThickness(2);
	convex.setOutlineColor(sf::Color::White);
	convex.setFillColor(sf::Color::Blue);

	//Petla glowna
	while (app.isOpen()){
		//Obsluga eventow
		Event event;
		while (app.pollEvent(event)){

			if (event.type == Event::Closed) {
				app.close();
			}

			if (event.key.code == Keyboard::Space) {
				Bullet *b = new Bullet();
				b->settings(sBullet, p->x, p->y, p->angle, 10);
				objects.push_back(b);
			}

			//Obsluga przyciskow dla menu
			if (menu.buttonAval) {
				if (event.type == Event::KeyReleased) {
					if (event.key.code == Keyboard::F2) {
						switchBool(paused);
						switchBool(menu.show);
						switchBool(menu.buttonAval);
					}

					if (event.key.code == Keyboard::F1) {
						switchBool(paused);
						switchBool(menu.showInfo);
						switchBool(menu.buttonAval);
					}

					if (event.key.code == Keyboard::Escape) {
						switchBool(paused);
						switchBool(exitprompt.show);
						switchBool(menu.buttonAval);
					}
				}
			}
			else if (event.type == Event::KeyReleased) {
				if (event.key.code == Keyboard::F4) {
					paused = false;
					menu.show = false;
					menu.showInfo = false;
					exitprompt.show = false;
					menu.buttonAval = true;
				}
			}


			//Obsluga menu wyjscia
			if (exitprompt.show) {
				switch (event.type) {
				case Event::KeyReleased:
					switch (event.key.code) {
					case Keyboard::Up:
						exitprompt.moveUp();
						break;

					case Keyboard::Down:
						exitprompt.moveDown();
						break;

					case Keyboard::Return:
						switch (exitprompt.getPressedItem()) {
						case 0:
							filemanager.save(p->points, p->lifes);
							app.close();
							break;
						case 1:
							switchBool(paused);
							switchBool(exitprompt.show);
							switchBool(menu.buttonAval);
							break;
						}
						break;
					}
					break;
				}
			}
			
			//Obsluga menu
			if (menu.show) {
				switch (event.type){
				case Event::KeyReleased:
					switch (event.key.code){

					case Keyboard::Up:
						menu.moveUp();
						break;

					case Keyboard::Down:
						menu.moveDown();
						break;

					case Keyboard::Return:
						switch (menu.getPressedItem()){
						case 0:
							//Wyjscie z menu
							switchBool(paused);
							switchBool(menu.show);
							switchBool(menu.buttonAval);
							break;
						case 1:
							//Zapisywanie gry
							filemanager.save(p->points, p->lifes);
							break;
						case 2:
							//Wczytywanie gry
							filemanager.load(p);
							lifesText.setString("Zycia: " + std::to_string(p->lifes));
							pointsText.setString("Punkty: " + std::to_string(p->points));
							break;
						case 3:
							//Zwiekszenie/zmniejszenie poziomu trudnosci
							if (menu.level<3) menu.level++;
							else menu.level = 1;
							menu.redraw(app.getSize().x, app.getSize().y);
							p->lifes = menu.level * 10;
							lifesText.setString("Zycia: " + std::to_string(p->lifes));
							break;
						case 4:
							//Zapisanie i wyjscie z gry
							filemanager.save(p->points, p->lifes);
							app.close();
							break;
						}
						break;
					}
					break;
				}
			}
		}

		//Jezeli gra nie jest zatrzymana, wykonuj obliczenia...--->
		if (!paused) {

			if (Keyboard::isKeyPressed(Keyboard::Right)) p->angle += 3;
			if (Keyboard::isKeyPressed(Keyboard::Left))  p->angle -= 3;
			if (Keyboard::isKeyPressed(Keyboard::Up)) p->thrust = true;
			else p->thrust = false;

			//Detekcja kolizji
			for (auto a : objects)
				for (auto b : objects){
					//Czy pocisk trafil asteroide?
					if (a->name == "asteroid" && b->name == "bullet")
						if (isCollide(a, b)){

							a->life = false;
							b->life = false;

							Object *e = new Object();
							e->settings(sExplosion, a->x, a->y);
							e->name = "explosion";
							objects.push_back(e);


							for (int i = 0;i < 2;i++){
								if (a->R == ASTER) continue;
								Object *e = new Asteroid();
								e->settings(sRock_small, a->x, a->y, rand() % 360, ASTER);
								objects.push_back(e);
							}

							//Aktualizacja tekstu dla licznika punktow
							p->points += 10;
							pointsText.setString("Punkty: " + std::to_string(p->points));
						}

					//Czy gracz zderzyl sie z asteroida?
					if (a->name == "player" && b->name == "asteroid")
						if (isCollide(a, b)){

							b->life = false;

							Object *e = new Object();
							e->settings(sExplosion_ship, a->x, a->y);
							e->name = "explosion";
							objects.push_back(e);

							p->settings(sPlayer, WIDTH / 2, HEIGHT / 2, 270, 20);
							p->dx = 0; p->dy = 0;

							p->lifes--;
							//Aktualizacja tekstu dla licznika zyc i wyswietlenie menu w przypadku zgonu
							lifesText.setString("Zycia: " + std::to_string(p->lifes));
							if (p->lifes <= 0) {
								paused = true;
								app.close();
							}
						}
				}

			//Poruszanie graczem
			if (p->thrust) p->anim = sPlayer_go;
			else p->anim = sPlayer;

			//Wykrywanie eksplozji
			for (auto e : objects)
				if (e->name == "explosion")
					if (e->anim.isEnd()) e->life = 0;

			//Tworzenie nowych asteroid
			if (rand() % (ASTER*10) == 0){
				Asteroid *a = new Asteroid();
				a->settings(sRock, 0, rand() % HEIGHT, rand() % 360, 25);
				objects.push_back(a);
			}

			//Aktualizacja obiektow
			for (auto i = objects.begin();i != objects.end();){
				Object *e = *i;

				e->update();
				e->anim.update();

				if (e->life == false) { i = objects.erase(i); delete e; }
				else i++;
			}

			//Rysowanie wszystkich elementow
			app.draw(background);
			app.draw(convex);
			for (auto i : objects)
				i->draw(app);	
			app.draw(lifesText);
			app.draw(pointsText);

		}
		//--->... w przeciwnym wypadku rysuj menu
		else {
			app.clear();
			app.draw(background);

			if(menu.show && !exitprompt.show)				
				menu.draw(app);				
			if (!menu.show && exitprompt.show)
				exitprompt.draw(app);
			if (menu.showInfo) 
				menu.showInfoScreen(app, app.getSize().x, app.getSize().y);
		}

		//Wyswietlenie wyrysowanych elementow na ekranie
		app.display();
	}
	return 0;
}


